import {Container, Col, Row, NavLink,} from 'react-bootstrap'
import FooterLogo from '../assets/media/icons/logofooter.jpg'
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext'

export default function Footer(){
	const {user} = useContext(UserContext)
	return(
		(user.isAdmin === true) ?
			<>
			</>
		:

		<Container fluid className="bg-color2 pt-5 px-5 text-light">
			<Row>
				<Col className="col-12 col-md-3">
					<NavLink className="text-light"><h5 className="mb-3"><strong>Collections</strong></h5></NavLink>
					<NavLink as={NavLink} to="/collection/allproducts" className="admin-nav"><p>All Products</p></NavLink>
					<NavLink as={NavLink} to="/collection/Laptop"><p className="admin-nav">Laptop</p></NavLink>
					<NavLink as={NavLink} to="/collection/Desktop"><p className="admin-nav">Destop</p></NavLink>
					<NavLink as={NavLink} to="/collection/Phone"><p className="admin-nav">Phones</p></NavLink>
					<NavLink as={NavLink} to="/collection/Tablet"><p className="admin-nav">Tablets</p></NavLink>
					<NavLink as={NavLink} to="/collection/Accessories"><p className="admin-nav">Accessories</p></NavLink>
					<NavLink as={NavLink} to="/collection/SmartHome"><p className="admin-nav">Smart Home</p></NavLink>
				</Col>
				<Col className="col-12 col-md-3">
					<NavLink className="text-light"><h5 className="mb-3"><strong>Links</strong></h5></NavLink>
					<NavLink><p className="admin-nav">About Us</p></NavLink>
					<NavLink><p className="admin-nav">Career</p></NavLink>
					<NavLink><p className="admin-nav">Terms of Service</p></NavLink>
					<NavLink><p className="admin-nav">User Privacy Policy</p></NavLink>
					<NavLink><p className="admin-nav">Refund Policy</p></NavLink>
					<NavLink><p className="admin-nav">Do not sell my personal information</p></NavLink>
					<NavLink><p className="admin-nav">Partners</p></NavLink>
				</Col>
				<Col className="col-12 col-md-3">
					<NavLink className="text-light"><h5 className="mb-3"><strong>Need help?</strong></h5></NavLink>
					<NavLink><p className="admin-nav">Contact Us</p></NavLink>
					<NavLink><p className="admin-nav">Track my Orders</p></NavLink>
					<NavLink><p className="admin-nav">FAQ's</p></NavLink>
				</Col>
				<Col className="col-12 col-md-3 d-inline-flex">
					<img src={FooterLogo} className="img-fluid col-5" id="footer-logo"/>
					<div className="mt-5 col-7">
						<h1 className="text-light col-12 mt-3"><strong>Caccah Shopping</strong></h1>
						<div className="d-inline-flex">
							<h6 className="text-light me-2">Follow us:</h6>
							<h5 className="fa fa-instagram mx-1 admin-nav mt-1"></h5>
							<h5 className="fa fa-facebook mx-1 admin-nav mt-1"></h5>
							<h5 className="fa fa-pinterest mx-1 admin-nav mt-1"></h5>
							<h5 className="fa fa-twitter mx-1 admin-nav mt-1"></h5>
						</div>
					</div>
				</Col>
				<p className="text-center body-text mt-5 mb-2">2023 Caccah Shopping by: <a href="https://obligado86.github.io/project-portfolio/#profile" target="_blank" className="admin-nav">Joseph Obligado</a>&#169;</p>
			</Row>
		</Container>
	)
}