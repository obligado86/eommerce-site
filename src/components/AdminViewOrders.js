import {useEffect, useState, useContext} from 'react';
import {Container, ListGroup, Card, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminViewOrder({order}){
	const { _id, userId, products, shippingCost, totalAmount, paymentMethod, status, trackingInfo, purchaseOn } = order;
		const {user} = useContext(UserContext);
		const [userEmail, setUserEmail] = useState('')
		const [userfirstName, setUserFristName] = useState('')
		const [userLastName, setUserLastName] = useState('')
		const [displayStatus, setDisplayStatus] = useState('')
		const [buttonChange, setButtonChange] = useState(true)
		const [buttonDisabled, setButtonDisabled] = useState(false)
		const displayShipping = shippingCost
		const displayTotalPrice = totalAmount.toLocaleString()
		const displayProductPrice = products[0].productPrice.toLocaleString()
		const displayOrderNum = _id
		const dataId = _id.slice(20, _id.length);

		const [trackinNum, setTrackinNum] = useState('')
		const [inputId, setInputId] = useState('')

		useEffect(() => {
			if(status === "pending"){
				setButtonDisabled(false)
				setButtonChange(true)
				setDisplayStatus("to Pay")
			} else if(status === "processsing"){
				setDisplayStatus("to ship")
				setButtonChange(false)
				setButtonDisabled(false)
			} else if(status === "shippedout"){
				
				setButtonDisabled(false)
				setDisplayStatus("to receive")
			} else if(status === "delivered"){
				setDisplayStatus("to rate")
			} else if(status === "returned"){
				setButtonDisabled(true)
				setDisplayStatus("canceled")
			} else if(status === "for cancelation"){
				setButtonDisabled(true)
				setDisplayStatus("canceled")
			} else if(status === "canceled"){
				setButtonDisabled(true)
				setDisplayStatus("canceled")
			}
			
		}, [status, products, _id])

		useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/admin/${userId}/userDetails`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			}).then(res => res.json()).then(data => {
				setUserEmail(data.email);
				setUserFristName(data.firstName);
				setUserLastName(data.lastName);
			}).catch(err => console.log(err))
		}, [userId])

		function confirmOrder(id){
			Swal.fire({
				title: "Are you sure to confirm this order?",
				icon: "warning",
				confirmButtonText: 'Confirm',
				showCancelButton: true
			}).then((result) => {
				if(result.isConfirmed){
					fetch(`${process.env.REACT_APP_API_URL}/admin/${id}/confirm`, {
						method: 'PATCH'
					}).then(res => res.json()).then(data => {
						if (data) {
							Swal.fire({
								title: "Your order is now for on Process",
								icon:"success"
							})
						}
					}).catch(err => console.log(err))
				}
			})
		}

		function confirmShipping(e){
			e.preventDefault();
			fetch(`${process.env.REACT_APP_API_URL}/admin/${_id}/toship`, {
				method: 'PATCH',
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					trackingInfo: trackinNum
				})
			}).then(res => res.json()).then(data => {
				if(data){
					Swal.fire({
						title: "Order is now for delivery",
						icon: "success",
						text: `with tracking number: ${trackinNum}`
					})
				} else {
					Swal.fire({
						title: "fail request",
						icon: "error",
					})
				}
			}).catch(err => console.log(err))
		}

		return (
			<Card className="my-3" id={`card-${dataId}`}>
			<span className="d-block d-md-inline-flex justify-content-between">
				<p className="mt-2 ms-2"><strong>Order No:</strong> {displayOrderNum}<br/><strong>User:</strong> {userId}<br/>
				<strong>User email:</strong> {userEmail} <strong>User Name:</strong> {userfirstName} {userLastName}</p>
				{ buttonChange ?
					<>
						{ !buttonDisabled ?
							<Button onClick={() => confirmOrder(_id)} className="btn btn-success mx-4 mt-4">Fulfill Order</Button>
						:
							<Button className="btn btn-secondary mx-4 mt-4" disabled>Fulfill Order</Button>
						}
					</>
				:
					<>
						{ !buttonDisabled ?
							<Form onSubmit={(e) => confirmShipping(e)} className="form-inline mx-4 mt-md-4 d-inline-flex">
								<Form.Group className="position-absolute d-none" controlId="orderid">
								    <Form.Control 
								        type="text" 
								        value={_id}
								        onChange={e => setInputId(e.target.value)}
								        required/>
								</Form.Group>
								<Form.Group className="d-inline-flex w-100" controlId="trackingNum">
									<Form.Control 
								        type="text" 
								        placeholder="Tranking number"
								        value={ trackinNum }
								        onChange={e => setTrackinNum(e.target.value)}
								        className="input-group form-control" 
								        required/>
								</Form.Group>
								<Button className="btn btn-success" type="submit">Ship Order</Button>
							</Form>
						:
							<Button className="btn btn-secondary mx-4 mt-4" disabled>Ship Orders</Button>
						}
					</>
				}
			</span>
			<p className="mb-2 ms-2"><strong>Tracking No:</strong> {trackingInfo}</p>
			<ListGroup horizontal as="ol" numbered className="col-12" >
					<ListGroup.Item className="col-3">Status:<br/> {displayStatus}</ListGroup.Item>
					<ListGroup.Item className="col-3">Payment Option:<br/> {paymentMethod}</ListGroup.Item>
					<ListGroup.Item className="col-3">Date Ordered:<br/> {purchaseOn}</ListGroup.Item>
					<ListGroup.Item className="col-3">Shipping Cost: &#8369;{displayShipping}.00<br/><strong>Total Amount:</strong> &#8369;{displayTotalPrice}.00</ListGroup.Item>	
			</ListGroup>
			<p className="m-3"><strong>Products:</strong></p>
			<ListGroup horizontal as="ol" numbered className="col-12" >
				<ListGroup.Item className="col-2"><img src={products[0].productImage} className="img-thumbnail w-100"/></ListGroup.Item>
				<ListGroup.Item as={Link} to={`/collection/product/${products[0].productId}`} className="col-4 text-dark">{products[0].productName}</ListGroup.Item>
				<ListGroup.Item className="col-2">Qty: {products[0].quantity}</ListGroup.Item>
				<ListGroup.Item className="col-4">Item Price: &#8369;{displayProductPrice}.00</ListGroup.Item>
			</ListGroup>
			</Card>
		)
}