import {useEffect, useState, useContext} from 'react' 
import {ListGroup, Container, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

//import ProductOrderList from './ProductOrderList'
//import UserContext from '../UserContext'

export default function UserViewOrders({order}) {
	const { _id, userId, products, shippingCost, totalAmount, paymentMethod, status, trackingInfo, purchaseOn } = order;

	const [displayStatus, setDisplayStatus] = useState('')
	const [buttonChange, setButtonChange] = useState(true)
	const [buttonDisabled, setButtonDisabled] = useState(false)
	const displayShipping = shippingCost
	const displayTotalPrice = totalAmount.toLocaleString()
	const displayProductPrice = products[0].productPrice.toLocaleString()
	const displayOrderNum = _id + "-0000" + purchaseOn.slice(20, purchaseOn.length)

	useEffect(() => {
		if(status === "pending"){
			setButtonDisabled(false)
			setDisplayStatus("to Pay")
		} else if(status === "processsing"){
			setDisplayStatus("to ship")
			setButtonDisabled(true)
		} else if(status === "shippedout"){
			setButtonChange(false)
			setButtonDisabled(false)
			setDisplayStatus("to receive")
		} else if(status === "delivered"){
			setDisplayStatus("to rate")
		} else if(status === "returned"){
			setButtonDisabled(true)
			setDisplayStatus("canceled")
		} else if(status === "for cancelation"){
			setButtonDisabled(true)
			setDisplayStatus("canceled")
		} else if(status === "canceled"){
			setButtonDisabled(true)
			setDisplayStatus("canceled")
		}
		
	}, [status, products, _id])



	function cancelOrder(id){
		Swal.fire({
			title: "Are you sure to cancel the order?",
			icon: "warning",
			confirmButtonText: 'Confirm',
			showCancelButton: true
		}).then((result) => {
			if(result.isConfirmed){
				fetch(`${process.env.REACT_APP_API_URL}/${id}/cancel`, {
					method: 'PATCH'
				}).then(res => res.json()).then(data => {
					if (data) {
						Swal.fire({
							title: "Your order is now for cancelation",
							icon:"success"
						})
					}
				}).catch(err => console.log(err))
			}
		})
	}

	return (
		<Card className="my-3">
		<span className="d-inline-flex justify-content-between">
			<p className="mt-2 ms-2"><strong>Order No:</strong> {displayOrderNum}</p>
			{ buttonChange ?
				<>
					{ !buttonDisabled ?
						<Button onClick={() => cancelOrder(_id)} className="btn btn-danger mx-4 mt-4">Cancel Order</Button>
					:
						<Button className="btn btn-secondary mx-4 mt-4" disabled>Cancel Order</Button>
					}
				</>
			:
				<>
					{ !buttonDisabled ?
						<Button className="btn btn-success mx-4 mt-4">Order Recieved</Button>
					:
						<Button className="btn btn-secondary mx-4 mt-4" disabled>Order Recieved</Button>
					}
				</>
			}
		</span>
		<p className="mb-2 ms-2"><strong>Tracking No:</strong> {trackingInfo}</p>
		<ListGroup horizontal as="ol" numbered className="col-12" >
				<ListGroup.Item className="col-3">Status:<br/> {displayStatus}</ListGroup.Item>
				<ListGroup.Item className="col-3">Payment Option:<br/> {paymentMethod}</ListGroup.Item>
				<ListGroup.Item className="col-3">Date Ordered:<br/> {purchaseOn}</ListGroup.Item>
				<ListGroup.Item className="col-3">Shipping Cost: &#8369;{displayShipping}.00<br/><strong>Total Amount:</strong> &#8369;{displayTotalPrice}.00</ListGroup.Item>	
		</ListGroup>
		<p className="m-3"><strong>Products:</strong></p>
		<ListGroup horizontal as="ol" numbered className="col-12" >
			<ListGroup.Item className="col-2"><img src={products[0].productImage} className="img-thumbnail w-100"/></ListGroup.Item>
			<ListGroup.Item as={Link} to={`/collection/product/${products[0].productId}`} className="col-4 text-dark">{products[0].productName}</ListGroup.Item>
			<ListGroup.Item className="col-2">Qty: {products[0].quantity}</ListGroup.Item>
			<ListGroup.Item className="col-4">Item Price: &#8369;{displayProductPrice}.00</ListGroup.Item>
		</ListGroup>
		</Card>
	)
}
